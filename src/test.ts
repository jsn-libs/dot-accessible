import {DotAccessible} from "./index";
import * as util from "util";

const obj = {
    a: {
        b: {
            c: {
                d: {
                    e: {
                        chair: {
                            color: 'brown',
                            weight: 45
                        }
                    }
                },
                and: {
                    table: {
                        color: 'black',
                        weight: 120
                    }
                }
            }
        }
    }
};

const map = DotAccessible.flatten(obj);
const out = DotAccessible.unflatten(map);

console.log(map);
console.log(util.inspect(out, false, Infinity));
