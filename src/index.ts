import {flatten} from "./flatten";
import {unflatten} from "./unflatten";

/**
 * @type {{flatten: <T>(obj: object) => Map<string, T>, unflatten: <T>(obj: (object | Map<string, T>)) => object}}
 */
export const DotAccessible = {
    flatten: flatten,
    unflatten: unflatten
};
