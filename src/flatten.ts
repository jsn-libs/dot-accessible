/**
 * @param {object} obj
 * @return {Map<string, T>}
 */
export function flatten<T>(obj: object): Map<string, T>{
    const flattened = new Map<string, T>(),
        stack: any = [[obj, null]];

    while(stack.length > 0){
        const [o, pKey] = stack.pop();

        for(const key of Object.keys(o)){
            const sKey = pKey !== null ? pKey + '.' + key : key;

            if(typeof o[key] === 'string' || typeof o[key] === 'number' || typeof o[key] === 'boolean'
                || (typeof o[key] === 'object' && Array.isArray(o[key])) || o[key] === null){
                flattened.set(sKey, o[key]);
            }else if(typeof o[key] === 'object'){
                stack.push([o[key], sKey]);
            }
        }
    }

    return flattened;
}
