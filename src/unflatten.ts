/**
 * @param {Map<string, T>} map
 * @return {object}
 */
function unflatten_map<T>(map: Map<string, T>): object{
    const obj: any = {};

    for(const [key, value] of map.entries()){
        const key_chain = key.split('.');

        for(let c = obj, i = 0; i < key_chain.length; i++){
            if(i < key_chain.length - 1){
                if(!c[key_chain[i]])
                    c[key_chain[i]] = {};
            }else{
                c[key_chain[i]] = value;
            }

            if(i < key_chain.length - 1)
                c = c[key_chain[i]];
        }
    }

    return obj;
}

/**
 * @param {object} obj
 * @return {object}
 */
function unflatten_object<T>(obj: object): object{
    const out: any = {};

    for(const [key, value] of Object.entries(obj)){
        const key_chain = key.split('.');

        for(let c = out, i = 0; i < key_chain.length; i++){
            if(i < key_chain.length - 1){
                if(!c[key_chain[i]])
                    c[key_chain[i]] = {};
            }else{
                c[key_chain[i]] = value;
            }

            if(i < key_chain.length - 1)
                c  = c[key_chain[i]];
        }
    }

    return out;
}

/**
 * @param {object | Map<string, T>} obj
 * @return {object}
 */
export function unflatten<T>(obj: object | Map<string, T>): object{
    if(obj instanceof Map)
        return unflatten_map<T>(obj);

    return unflatten_object<T>(obj);
}
